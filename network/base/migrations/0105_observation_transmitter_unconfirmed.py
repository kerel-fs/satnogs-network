# Generated by Django 4.0.6 on 2023-03-15 12:05

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('base', '0104_observation_transmitter_status'),
    ]

    operations = [
        migrations.AddField(
            model_name='observation',
            name='transmitter_unconfirmed',
            field=models.BooleanField(blank=True, null=True),
        ),
    ]
